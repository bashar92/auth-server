const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStartegy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

// create Local Strategy
const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, function (
  email,
  password,
  done
) {
  // Verify email and password
  User.findOne({ email: email }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false);
    }

    // compare passwords
    user.comparePasswords(password, function (err, result) {
      if (err) {
        return done(err);
      }
      if (!result) {
        return done(null, false);
      } else {
        return done(null, user);
      }
    });
  });
});

// Setup options for jwtStrategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secret,
};

// Create JwtStrategy
const jwtLogin = new JwtStartegy(jwtOptions, function (
  payload /* decoded Jwt Token */,
  done
) {
  // see if the user ID in the payload exists in the database
  // If does, call 'done' with the found user
  // If not, call 'done' without a user
  User.findById(payload.sub, function (err, user) {
    if (err) {
      return done(err);
    }

    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
    }
  });
});

// Tell passport to use the created Strategy
passport.use(jwtLogin);
passport.use(localLogin);
