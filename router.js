const AuthenticationController = require('./controllers/AuthController');
const passportService = require('./services/passport');
const passport = require('passport');

// Authenticated Users already obtained a token to access protected resources
const requireAuth = passport.authenticate('jwt', { session: false });

// Users need to sign in with valid email, and password to obtain a token
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function (app) {
  app.get('/', requireAuth, function (req, res) {
    res.send({ author: 'Khdr Bashar' });
  });

  app.post('/api/signup', AuthenticationController.Signup);
  app.post('/api/signin', requireSignin, AuthenticationController.Signin);
};
