const express = require('express');
const http = require('http');
const morgan = require('morgan'); // Logging Framework
const bodyParser = require('body-parser');
const router = require('./router');
const mongoose = require('mongoose');
const app = express();

// App setup
app.use(morgan('combined'));
app.use(bodyParser(bodyParser.json({ type: '*/*' })));
router(app);

// Server setup
const port = process.env.PORT || 8085;
const server = http.createServer(app);
server.listen(port);
console.log(`Server listening on port: ${port}`);

// DB Setup
mongoose
  .connect('mongodb://localhost/auth', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('connected to MonogoDB');
  })
  .catch((err) => console.error(err));
