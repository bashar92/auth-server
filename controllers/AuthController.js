const User = require('../models/user');
const jwt = require('jwt-simple');
const config = require('../config');

module.exports = {
  Signup: (req, res, next) => {
    const user = req.body;
    // See if user with a given email exists

    User.findOne({ email: user.email })
      .then((existingUser) => {
        // if email is existed, then return an error
        if (existingUser) {
          return res
            .status(422) // entity cannot be processed
            .send({ code: 422, message: 'Email address is already in user!' });
        }
      })
      .catch((err) => {
        return next(err);
      });
    // if email does not exist, then create a new user record
    const newUser = new User({ email: user.email, password: user.password });

    newUser
      .save(newUser)
      .then((user) => {
        // return a success message.
        res.status(200).send({
          code: 200,
          message: 'user is created successfully!',
          token: generateToken(user),
        });
      })
      .catch((err) => {
        return res
          .status(422)
          .send({ code: 422, message: err._message, errors: err.errors });
      });
  },
  Signin: (req, res, next) => {
    const user = req.user;
    res.send({ token: generateToken(user) });
  },
};

function generateToken(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}
