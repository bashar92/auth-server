const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

// Define the model

const userSchema = new schema({
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },
  password: { type: String, required: true },
});

// On save hook , encrypt password
// before saveing a model, run the following function
userSchema.pre('save', function (next) {
  const user = this; // The context of this function in the user model
  bcrypt.genSalt(10, function (err, salt) {
    if (err) {
      return next(err);
    }

    bcrypt.hash(user.password, salt, null, function (err, hash) {
      if (err) {
        return next(err);
      }

      user.password = hash;

      next();
    });
  });
});

userSchema.methods.comparePasswords = function (candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function (err, result) {
    if (err) {
      return callback(err);
    }

    return callback(null, result);
  });
};

// Create the model Class
const model = mongoose.model('user', userSchema);

//export the model
module.exports = model;
